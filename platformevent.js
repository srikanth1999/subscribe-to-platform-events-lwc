import {
  LightningElement,
  track,
  wire,
  api
} from 'lwc';
import {
  ShowToastEvent
} from "lightning/platformShowToastEvent";
import {
  NavigationMixin
} from 'lightning/navigation';
import {
  subscribe,
  unsubscribe,
  onError
} from 'lightning/empApi';
import {
  registerListener,
  unregisterAllListeners,
  fireEvent
} from "c/pubsub";
import {
  CurrentPageReference
} from "lightning/navigation";
export default class OrcaSaleSuccess extends NavigationMixin(LightningElement) {
  @wire(CurrentPageReference) pageRef;

  subscription = {};
  CHANNEL_NAME = '/event/Opportunity_Closed__e';
  connectedCallback() {
    console.log('Start');
    subscribe(this.CHANNEL_NAME, -1, this.refreshList).then(response => {
      console.log(response);
      this.subscription = response;
    });
    onError(error => {
      console.error('Server Error--->' + error);
    });
    this.refresh();
  }

  refreshList = (response) => {
    console.log('reading platform event', JSON.stringify(this.subscription));
  }
}